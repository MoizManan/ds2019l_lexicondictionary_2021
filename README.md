# Lexicon Dictionary 
# Description:
In today’s world, everyone is comfortable with online searching for every little thing. We need to 
search for something almost every day, often meanings of some words to make our document look 
professional. ‘Lexicon’ is an interactive dictionary website where anyone can perceive the 
meanings of words they do not comprehend along with daily quotations to whip up users. 
Moreover, one can bookmark their favourite words and quotes. An engaging game and quiz has 
also been made through which user can judge their understanding of language. A noteworthy facet 
of this website is that one can search and bookmark Urdu language words with meanings. There 
is circadian word of the day (English and Urdu) and quotes to boot.
# Configuration Steps:
### Step No: 1 
Install [Visual Studio Code](https://code.visualstudio.com/download) 
& [XAMPP.](https://www.apachefriends.org/download.html)
### Step No: 2
Download these files into your PC and save these files in a same folder. So that, there may not be any issue regarding paths.
### Step No: 3
Open XAMPP. From 'db' folder in the downloaded files, import database with name 'dict'.
### Step No: 4
Open the source code (app.py) from VScode.
### Step No: 5
In the terminal window, press Ctrl+C. now write (.\env\Scripts\Activate.ps1) to active the code.
Similarly for deactivating it, write (deactivate).
### Step No: 6
When you activated the code, the main webpage (home-page) is opened in your default browser. Now you can check the dictionary website and its amazing features there.
### Step No: 7 (Deployment Step)
Here's the link of our website [Lexicon.](http://lexicon-dictionary.herokuapp.com/)
### Step No: 8
Here's live demo of our [Project!](https://www.youtube.com/watch?v=yLq_N2zRXz4)
