import datetime
import  random
from logging import debug
from os import stat
from flask import *
from flask_mysqldb import MySQL
from random import randint 
import smtplib; 

from notifypy import Notify

notification = Notify()



class Node():

    def __init__(self, data=None):
        self.data = data
        self.next= None

class LinkedList():
    def __init__(self):
        self.head = None
        
    def isEmptyLL(self):
        return self.head==None

    def insertAtHead(self, x):
        new_node = Node(x)
        new_node.next=self.head
        self.head = new_node

  
    def findNode(self,x):
        temp=self.head
        while(temp!=None):
            if(temp.data.split(":")[0]==x):
                return temp.data.split(":")[1]
            temp=temp.next
        return False
            
            

lofurduwords=[]
luwwms=LinkedList()
with open("urdudicr.txt") as f:
    
    for x in f:
        if x!="" and x!="\n":
         lofurduwords.append(x.rstrip("\n"))
         luwwms.insertAtHead(x.rstrip("\n"))

app = Flask(__name__)

app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = ''
app.config['MYSQL_DB'] = 'dict'

mysql=MySQL(app)

otp=""
mail=""

def send_email(from_addr, to_addr_list,message,login, password, smtpserver='smtp.gmail.com:587'):
    server = smtplib.SMTP(smtpserver)
    server.starttls()
    server.login(login,password)
    server.sendmail(from_addr, to_addr_list,message)
    server.quit()


@app.route('/',)
def hello_world():
    tdate=datetime.date.today()
    word=""
    wordtype=""
    definition=""
    cursor=mysql.connection.cursor()
    rs=cursor.execute(f"SELECT * FROM wordofday WHERE DATE='{tdate}'") 
    if rs>0:
        r=cursor.fetchall()
        word=r[0][0]
        wordtype=r[0][1]
        definition=r[0][2]
        cursor.close()

    else:
        rwq=cursor.execute("SELECT * FROM ENTRIES ORDER BY RAND() LIMIT 1")
        rwr=cursor.fetchall()
        word=rwr[0][0]
        wordtype=rwr[0][1]
        definition=rwr[0][2]
        check=cursor.execute(f"SELECT * FROM wordofday WHERE word='{word}'")
        if check<=0:
            cursor.execute(f"INSERT INTO wordofday(word,wordtype,definition,DATE) VALUES('{word}','{wordtype}','{definition}','{tdate}')")
            mysql.connection.commit()
            cursor.close()
        else:
            q=cursor.execute("SELECT * FROM ENTRIES ORDER BY RAND() LIMIT 1")
            randomrs=cursor.fetchall()
            word=randomrs[0][0]
            wordtype=randomrs[0][1]
            definition=randomrs[0][2]
            cursor.execute(f"INSERT INTO wordofday(word,wordtype,definition,DATE) VALUES('{word}','{wordtype}','{definition}','{tdate}')")
            mysql.connection.commit()
            cursor.close()
    
    quote=""
    cursor2=mysql.connection.cursor() 
    rs2=cursor2.execute(f"SELECT * FROM quoteofday WHERE DATE='{tdate}'")
    if rs2>0:
        r=cursor2.fetchall()
        quote=r[0][0]
        cursor2.close()
    else:
        rqq=cursor2.execute("SELECT * FROM quotes1 ORDER BY RAND() LIMIT 1")
        rqr=cursor2.fetchall()
        quote=rqr[0][3]
        check2=cursor2.execute(f"SELECT * FROM quoteofday WHERE quote='{quote}'")
        if check2<=0:
            cursor2.execute(f"INSERT INTO quoteofday(quote,DATE) VALUES('{quote}','{tdate}')")
            mysql.connection.commit()
            cursor2.close()
        else:
            q=cursor2.execute("SELECT * FROM quotes1 ORDER BY RAND() LIMIT 1")
            randomqs=cursor2.fetchall()
            quote=randomqs[0][3]
            cursor2.execute(f"INSERT INTO quoteofday(quote,DATE) VALUES('{quote}','{tdate}')")
            mysql.connection.commit()
            cursor2.close()

    ruwbs=''
    lruwwm=[]
    ruw=''
    rum=""
    cursor3=mysql.connection.cursor() 
    rs3=cursor3.execute(f"SELECT * FROM urduwordfday WHERE DATE='{tdate}'")
    if rs3>0:
        r=cursor3.fetchall()
        ruw=r[0][0].strip()
        rum=r[0][1].strip()
        cursor3.close()
    else:
        ruwbs=random.choice(lofurduwords)
        lruwwm=ruwbs.split(":")
        ruw=lruwwm[0].strip()
        rum=lruwwm[1].strip()
        check3=cursor3.execute(f"SELECT * FROM urduwordfday WHERE word='{ruw}'")
        if check3<=0:
            cursor3.execute(f"INSERT INTO urduwordfday(word,meaning,DATE) VALUES('{ruw}','{rum}','{tdate}')")
            mysql.connection.commit()
            cursor3.close()
        else:
            ruwbs=random.choice(lofurduwords)
            lruwwm=ruwbs.split(":")
            ruw=lruwwm[0].strip()
            rum=lruwwm[1].strip()
            cursor3.execute(f"INSERT INTO urduwordfday(word,meaning,DATE) VALUES('{ruw}','{rum}','{tdate}')")
            mysql.connection.commit()
            cursor3.close()


    return render_template("mainwindow.html",word=word,wordtype=wordtype,definition=definition,quote=quote,ruw=ruw,rum=rum,username=username,emailuser=emailuser)

@app.route('/game',)
def game(): 
    lofwords=[]
    cursor=mysql.connection.cursor()    
    rs=cursor.execute(f"SELECT word FROM ENTRIES WHERE length(word)>3 ORDER BY RAND() LIMIT 200")
    words=cursor.fetchall()
    for i in range(len(words)):
        lofwords.append(words[i][0])
    return render_template("game.html",lofwords=lofwords);  

            


@app.route('/logorreg',)
def logorReg():
    return render_template("login1.html")

username=""
emailuser=""
@app.route('/login',methods=['POST'])
def login():
     if request.method=='POST':
        log=""
        email=request.form['email']
        password=request.form['pass']
        date=datetime.datetime.now()
        cursor=mysql.connection.cursor()    
        rs=cursor.execute(f"SELECT * FROM ACCOUNTS WHERE EMAIL='{email}' and PASSWORD='{password}'")
        if rs>0:
            cre=cursor.fetchall()
            global username
            global emailuser
            username=cre[0][1]
            emailuser=cre[0][2]
            cursor.execute(f"SELECT ID FROM ACCOUNTS WHERE EMAIL='{email}' and PASSWORD='{password}'")
            rs=cursor.fetchall()
            print(rs[0][0])
            cursor.execute(f"INSERT INTO LOGS(ID,DATE) VALUES({rs[0][0]},'{date}')")
            mysql.connection.commit()
            cursor.close()
            log+="yes"
            notification.title = "Successful"
            notification.message = f"Welcome back,{username}"
            notification.icon = "C:\\Users\\Moiz Manan\\Desktop\\Flask\\static\\read (4).png"
            notification.send()
            return redirect (url_for("hello_world"))
        elif rs==0:
            cursor.close()
            log+="no"
            return render_template("login1.html",log=log)

@app.route('/signup',methods=['POST'])
def signup():
    if request.method=='POST':
        status=""
        name=request.form['name']
        email=request.form['email']
        password=request.form['pass']
        con=request.form['country']
        date=datetime.datetime.now()
        cursor=mysql.connection.cursor()    
        rs=cursor.execute(f"SELECT * FROM ACCOUNTS WHERE EMAIL='{email}'")
        if rs==0:
            cursor.execute(f"INSERT INTO ACCOUNTS(NAME,EMAIL,PASSWORD,COUNTRY,DATE) VALUES('{name}','{email}','{password}','{con}','{date}')")
            mysql.connection.commit()
            cursor.close()
            status+="yes"
            notification.title = "Regisstered"
            notification.message = "Account created. Kindly log in to continue."
            notification.icon = "C:\\Users\\Moiz Manan\\Desktop\\Flask\\static\\read (4).png"
            notification.send()
            return(render_template("login1.html",status=status))
        elif rs > 0:
            cursor.close()
            status+="no"
            return render_template("login1.html",status=status)
            
@app.route('/verify')
def verify():
    return render_template("verify.html")

@app.route('/otp',methods=['POST'])
def otp():
    if request.method=='POST':
        found=""
        email=request.form['email']
        cursor=mysql.connection.cursor()    
        rs=cursor.execute(f"SELECT * FROM ACCOUNTS WHERE EMAIL='{email}'")
        if rs>0:
            global otp
            global mail
            mail=email
            otp=f"{randint(100,999)}-{randint(100,999)}"
            msg=f"Dear Sir,\n\t\tWe are glad that your account can be recovered. Your verification code is: {otp}"
            send_email("lexiconcommunity",f"{email}",msg,"lexiconcommunity@gmail.com","lexicon123..")
            found+="yes"
            cursor.close()
            return(render_template("verify.html",found=found))
        elif rs ==0:
            found+="no"
            cursor.close()
            return render_template("verify.html",found=found)

@app.route('/otp2',methods=['POST'])
def otp2():
    if request.method=='POST':
        correct=""
        vc=request.form['otp'] 
        global otp  
        if otp==vc:
            return(render_template("newpass.html"))
        else:
            correct+="no"
            return render_template("verify.html",correct=correct)


@app.route('/newpass',methods=['POST'])
def newPass():
    if request.method=='POST':
        match=""
        p=request.form['npass']
        cp=request.form['cpass']
        global mail
        email=mail
        if p==cp:
            cursor=mysql.connection.cursor()    
            cursor.execute(f"UPDATE ACCOUNTS SET PASSWORD='{p}' WHERE EMAIL='{mail}'")
            mysql.connection.commit()
            cursor.close()
            return redirect(url_for('logorReg'))
        else:
            match+="no"
            return render_template("newpass.html",match=match)

@app.route('/quoteoftheday',)
def qod():
    lqodays=[]
    ldates=[]
    cursor=mysql.connection.cursor() 
    query=cursor.execute(f"SELECT * FROM quoteofday LIMIT 20")
    rs=cursor.fetchall()
    
    for  i in range(len(rs)):
        lqodays.append(rs[i][0])
        ldates.append(rs[i][1])
    length=len(lqodays)
    
    return render_template("qod.html",lqodays=lqodays,length=length,ldates=ldates);  

@app.route('/urduoftheday',)
def uod():
    luwodays=[]
    lum=[]
    ldates=[]
    cursor=mysql.connection.cursor() 
    query=cursor.execute(f"SELECT * FROM urduwordfday LIMIT 20")
    rs=cursor.fetchall()
    
    for  i in range(len(rs)):
        luwodays.append(rs[i][0])
        lum.append(rs[i][1])
        ldates.append(rs[i][2])
    length=len(luwodays)
    
    return render_template("uwod.html",luwodays=luwodays,length=length,ldates=ldates,lum=lum);  


@app.route('/wordoftheday',)
def wod():
    lwodays=[]
    lwm=[]
    ldates=[]
    cursor=mysql.connection.cursor() 
    query=cursor.execute(f"SELECT * FROM wordofday LIMIT 20")
    rs=cursor.fetchall()
    
    for  i in range(len(rs)):
        lwodays.append(rs[i][0])
        lwm.append(rs[i][2])
        ldates.append(rs[i][3])
    length=len(lwodays)
    
    return render_template("wod.html",lwodays=lwodays,length=length,ldates=ldates,lwm=lwm);  
   

@app.route('/search',methods=['POST'])
def search():
    w=''
    meaning=''
    wordtype=''
    if request.method=='POST':
        word=request.form['search'] 
        cursor=mysql.connection.cursor() 
        query=cursor.execute(f"SELECT * FROM entries WHERE word='{word.capitalize()}'")
        rs=cursor.fetchall()
        if query>0:
            w=rs[0][0]
            meaning=rs[0][2]
            wordtype=rs[0][1]
        else:
            w=word
            meaning=luwwms.findNode(word)
            wordtype="urdu"
        return render_template("search.html",w=w,meaning=meaning,wordtype=wordtype); 
    
    
def quizdict():
    d={}
    cursor=mysql.connection.cursor()
    query=cursor.execute(f"SELECT word,definition FROM ENTRIES WHERE length(word)>3 ORDER BY RAND() LIMIT 200")
    rs=cursor.fetchall()
    for i in range(len(rs)):
        d.update({rs[i][0]:rs[i][1]})
    return d
    

@app.route('/quiz',)
def quiz():
    d=quizdict()
    return render_template("quiz.html",d=d)

@app.route('/profile',)
def profile():
    
    return render_template("profile.html",username=username,emailuser=emailuser)

@app.route('/logout',)
def logout():
    global username
    global emailuser
    username=""
    emailuser=""
    return redirect (url_for("hello_world"))


@app.route('/bookmark1',methods=['POST'])
def bkmark1():
    global username
    global emailuser
    cursor=mysql.connection.cursor()
    if(username!='' and emailuser!='') and request.method=='POST':
        word=request.form['bwod']
        cursor.execute(f"INSERT INTO bookmarks(NAME,EMAIL,WORD) VALUES('{username}','{emailuser}','{word}')")
        mysql.connection.commit()
        cursor.close()
        notification.title = "Successful"
        notification.message = "Word bookmarked."
        notification.icon = "C:\\Users\\Moiz Manan\\Desktop\\Flask\\static\\read (4).png"
        notification.send()
    else:
        notification.title = "Log In"
        notification.message = "Dear user, kindly login first."
        notification.icon = "C:\\Users\\Moiz Manan\\Desktop\\Flask\\static\\read (4).png"
        notification.send()
    return redirect (url_for("hello_world"))


@app.route('/bookmark2',methods=['POST'])
def bkmark2():
    global username
    global emailuser
    cursor=mysql.connection.cursor()
    if(username!='' and emailuser!='') and request.method=='POST':
        quote=request.form['bqod']
        cursor.execute(f"INSERT INTO bookmarks(NAME,EMAIL,QUOTE) VALUES('{username}','{emailuser}','{quote}')")
        mysql.connection.commit()
        cursor.close()
        notification.title = "Successful"
        notification.message = "Quote bookmarked."
        notification.icon = "C:\\Users\\Moiz Manan\\Desktop\\Flask\\static\\read (4).png"
        notification.send()
    else:
        notification.title = "Log In"
        notification.message = "Dear user, kindly login first."
        notification.icon = "C:\\Users\\Moiz Manan\\Desktop\\Flask\\static\\read (4).png"
        notification.send()
    return redirect (url_for("hello_world"))

@app.route('/bookmark3',methods=['POST'])
def bkmark3():
    global username
    global emailuser
    cursor=mysql.connection.cursor()
    if(username!='' and emailuser!='') and request.method=='POST':
        urdu=request.form['buod']
        cursor.execute(f"INSERT INTO bookmarks(NAME,EMAIL,URDU) VALUES('{username}','{emailuser}','{urdu}')")
        mysql.connection.commit()
        cursor.close()
        notification.title = "Successful"
        notification.message = "Urdu word bookmarked."
        notification.icon = "C:\\Users\\Moiz Manan\\Desktop\\Flask\\static\\read (4).png"
        notification.send()
    else:
        notification.title = "Log In"
        notification.message = "Dear user, kindly login first."
        notification.icon = "C:\\Users\\Moiz Manan\\Desktop\\Flask\\static\\read (4).png"
        notification.send()
    return redirect (url_for("hello_world"))


@app.route('/savedwords',)
def sws():
    lws=[]
    lqs=[]
    luws=[]
    cursor=mysql.connection.cursor() 
    query=cursor.execute(f"SELECT * FROM bookmarks WHERE name='{username}'")
    rs=cursor.fetchall()
    
    for  i in range(len(rs)):
        lws.append(rs[i][2])
        lqs.append(rs[i][3])
        luws.append(rs[i][4])

    for i in range(len(lws)):
        if(lws[i]==None):
            lws[i]='aaaa'
            
    for i in range(len(luws)):
        if(luws[i]==None):
            luws[i]='aaaa'

    for i in range(len(lqs)):
        if(lqs[i]==None):
            lqs[i]='aaaa'
    
    length=len(rs)
    
    return render_template("saved.html",lws=lws,length=length,luws=luws,lqs=lqs);  

if __name__== "__main__":
    app.run(debug=True)