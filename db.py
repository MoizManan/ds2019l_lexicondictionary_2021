from logging import debug
from flask import *
from flask_mysqldb import MySQL
app = Flask(__name__)
app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = ''
app.config['MYSQL_DB'] = 'dict'

mysql=MySQL(app)
@app.route('/')
def hello():
    cursor=mysql.connection.cursor()    
    # cursor.execute("CREATE TABLE ACCOUNTS(ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY,NAME VARCHAR(100),EMAIL VARCHAR(200),PASSWORD VARCHAR(20),COUNTRY VARCHAR(200),DATE VARCHAR(1000))")
    # cursor.execute("CREATE TABLE LOGS(ID INT NOT NULL,DATE VARCHAR(1000))")
    # cursor.execute("CREATE TABLE wordofday(word VARCHAR(25),wordtype VARCHAR(20),definition TEXT, DATE VARCHAR(1000))")
    # cursor.execute("CREATE TABLE quoteofday(quote TEXT, DATE VARCHAR(1000))")
    # cursor.execute("CREATE TABLE urduwordfday(word VARCHAR(200), meaning TEXT,DATE VARCHAR(1000))")
    # cursor.execute("CREATE TABLE bookmarks(name VARCHAR(200), email VARCHAR(200), word TEXT, quote TEXT, urdu TEXT )")

    mysql.connection.commit()
    cursor.close()

if __name__== "__main__":
    app.run(debug=True)